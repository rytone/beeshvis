use bv::{Bits, BitsExt, BitSliceable};
use cpal::{
    StreamData,
    UnknownTypeOutputBuffer as UOut,
};
use png::HasParameters;
use rustfft::{
    num_complex::Complex,
    num_traits::Zero,
};
use sample::{Sample, signal::Signal};

use std::io::{BufRead, Write};

const FILENAME: &'static str = "80squares/master.wav";

const CH1: &'static str = "80squares/ch1.wav";
const CH2: &'static str = "80squares/ch2.wav";
const CH3: &'static str = "80squares/ch3.wav";

const FRAMERATE: u32 = 60;
const FRAME_LEN_MS: u32 = 50;
const MAX_CYCLE_MS: u32 = 200; // 10 hz

const DUMP_FRAMES: bool = true;

fn find_frame_shift(
    samples: &[i16],
    data_idx: usize,
    sample_rate: u32,
) -> (i32, usize, Vec<u32>) {
    let samples_per_frame = sample_rate * FRAME_LEN_MS / 1000;
    let samples_per_cycle = sample_rate * MAX_CYCLE_MS / 1000;

    // TODO actually fix the rest of the oob accesses
    let search_range = &samples[data_idx..(data_idx+samples_per_cycle as usize).min(samples.len())];

    /*
    // find all positive zero crossings over the cycle range
    let mut zero_crossings = Vec::new();
    for i in 0..samples_per_cycle as usize - 1 {
        if (search_range[i] as f32) <= 0. && (search_range[i + 1] as f32) > 0. {
            zero_crossings.push(i);
        }
    }

    // bail out if not enough zero crossings are present
    if zero_crossings.len() < 2 {
        return (0, 0);
    }

    // score all possible cycles
    let cycle_start = zero_crossings[0];
    let possible_cycle_ends = &zero_crossings[1..];
    let mut scores = possible_cycle_ends
        .par_iter()
        .map(|this_end| {
            let length = this_end - cycle_start;

            let this_cycle = &samples[data_idx + cycle_start..data_idx + *this_end];
            let next_cycle = &samples[data_idx + *this_end..data_idx + *this_end+length];

            let diff_sum = this_cycle
                .iter()
                .zip(next_cycle)
                .map(|(a, b)| (a - b).abs() as u32)
                .sum::<u32>();

            (length, diff_sum as usize / (samples_per_cycle as usize - length))
        })
        .collect::<Vec<_>>();

    // sort possible cycle lengths and pick best
    scores.as_mut_slice().par_sort_by(|a, b| a.1.cmp(&b.1));
    let (best_length, _) = scores.remove(0);
    // let best_length = 950;

    // find first peak within wave
    // let (peak_ofs, peak_v) = search_range[cycle_start..cycle_start+best_length]
    //    .iter()
    //    .enumerate()
    //    .max_by(|a, b| a.1.cmp(b.1))
    //    .unwrap_or((0, &0));
    // dbg!(peak_v);
    
    /*
    // find peak rising edge
    let (peak_rise, _) = zero_crossings
        .iter()
        .map(|n| {
            let mut idx = *n..best_length;
            for i in &mut idx {
                if search_range[i + 1] <= search_range[i] {
                    break
                }
            }
            (*n, search_range[idx.start])
        })
        .max_by(|a, b| a.1.cmp(&b.1))
        .unwrap_or((0, 0));
    */
    */

    // modified "bitstream autocorrelation" implementation
    // https://www.cycfi.com/2018/03/fast-and-efficient-pitch-detection-bitstream-autocorrelation/

    // initialize bitstream with zero crossing data
    let mut bits = bv::BitVec::<usize>::with_capacity(samples_per_cycle as u64);
    let mut crossing = false;
    for v in search_range {
        if *v < -100 {
            crossing = false;
        } else if *v > 100 {
            crossing = true;
        }

        bits.push(crossing);
    }

    // perform autocorrelation
    let corr_res = (0..samples_per_cycle as u64 / 2)
        .map(|i| {
            let min_window = 1024;
            let start_slice = bits.bit_slice(0..i.max(min_window));
            let lag_slice = bits.bit_slice(i..i+i.max(min_window));

            let xor = start_slice.bit_xor(lag_slice);

            // biased against longer periods
            (0..xor.block_len())
                .map(|i| xor.get_block(i).count_ones())
                .sum::<u32>() + i as u32 / 4
        })
        .collect::<Vec<_>>();

    // find first local maximum
    let mut est_index_range = 0usize..corr_res.len()-1;
    for i in &mut est_index_range {
        if corr_res[i + 1] < corr_res[i] {
            break;
        }
    }

    // find minimum
    let max_ofs = est_index_range.start;
    let est_index = corr_res[est_index_range]
        .iter()
        .enumerate()
        .min_by(|a, b| a.1.cmp(b.1))
        .unwrap_or((0, &0))
        .0 + max_ofs;

    /*
    // harmonic correction
    let sub_threshold = (0.5 * max_count as f64) as u32;
    let max_div = est_index / min_period as usize;
    for div in (0..max_div).rev() {
        let mut all_strong = true;
        let mul = 1. / div as f64;

        for k in 1..div {
            let sub_period = (k as f64 * est_index as f64 * mul) as usize;
            if corr_res[sub_period] > sub_threshold {
                all_strong = false;
                break;
            }
        }

        if all_strong {
            est_index = (est_index as f64 * mul) as usize;
            break;
        }
    }

    // find start edge
    let mut start_idx = 0;
    let mut prev = 0;
    while start_idx < samples_per_cycle as usize - 1 && search_range[start_idx] <= 0 {
        prev = search_range[start_idx];
        start_idx += 1;
    }
    let dy = search_range[start_idx] - prev + 1;
    let dx1 = -prev / dy;

    // find next edge
    let mut next_idx = est_index.saturating_sub(1);
    while next_idx < samples_per_cycle as usize - 1 && search_range[next_idx] <= 0 {
        prev = search_range[next_idx];
        next_idx += 1;
    }
    let dy = search_range[next_idx] - prev + 1;
    let dx2 = -prev / dy;

    let best_length = ((next_idx - start_idx) as i16 + (dx2 - dx1)) as usize;
    */
    let best_length = est_index;
   
    if best_length > 2 {
        // do the fft magic
        let mut fft_in = search_range[0..best_length]
            .iter()
            .cloned()
            .map(f32::from)
            .map(Complex::from)
            .collect::<Vec<_>>();
        let mut fft_out = vec![Complex::<f32>::zero(); fft_in.len()];

        let mut planner = rustfft::FFTplanner::new(false);
        let fft = planner.plan_fft(fft_in.len());
        fft.process(&mut fft_in, &mut fft_out);

        let res = (fft_out.remove(1).to_polar().1 / (2. * std::f32::consts::PI) * best_length as f32) as i32;

        // find shift to align center cycle
        ((-(samples_per_frame as i32 / 2) - res) % best_length as i32, best_length, corr_res)
    } else {
        (0, 0, corr_res)
    }
}

fn find_sdl_gl_driver() -> Option<u32> {
    for (index, item) in sdl2::render::drivers().enumerate() {
        if item.name == "opengl" {
            return Some(index as u32);
        }
    }
    None
}

fn main() {
    // audio playback setup
    let ev = cpal::EventLoop::new();

    println!("available audio devices:");
    let mut devices = cpal::output_devices()
        .enumerate()
        .map(|(n, dev)| {
            println!("{}: {}", n, dev.name());
            dev
        })
        .collect::<Vec<_>>();

    print!("\ndevice: ");
    std::io::stdout().flush().unwrap();
    let device_idx: usize = std::io::stdin().lock().lines().next().unwrap().unwrap().parse().unwrap();
    let device = devices.remove(device_idx);

    let mut supported_formats = device.supported_output_formats()
        .expect("could not query supported output formats");
    let sample_format = supported_formats.next()
        .expect("no output formats available")
        .with_max_sample_rate();
    let dst_sample_rate = sample_format.sample_rate.0;


    // audio reading setup
    let audio_reader = hound::WavReader::open(FILENAME)
        .expect("could not open wav file");
    let ch1_reader = hound::WavReader::open(CH1)
        .expect("could not open wav file");
    let ch2_reader = hound::WavReader::open(CH2)
        .expect("could not open wav file");
    let ch3_reader = hound::WavReader::open(CH3)
        .expect("could not open wav file");
    println!("decoding wav files...");
    
    // assume single sample channel for now
    // also assume the master is the same sample rate as the channels
    let sample_rate = audio_reader.spec().sample_rate;
    let samples = audio_reader.into_samples::<i16>()
        .collect::<Result<Vec<_>, _>>().expect("could not decode sample");
    let ch1_samples = ch1_reader.into_samples::<i16>()
        .collect::<Result<Vec<_>, _>>().expect("could not decode sample");
    let ch2_samples = ch2_reader.into_samples::<i16>()
        .collect::<Result<Vec<_>, _>>().expect("could not decode sample");
    let ch3_samples = ch3_reader.into_samples::<i16>()
        .collect::<Result<Vec<_>, _>>().expect("could not decode sample");
    
    println!("done.");


    // actually start listening for audio
    let stream = ev.build_output_stream(&device, &sample_format)
        .expect("could not build output stream");

    let samples_per_frame = sample_rate * FRAME_LEN_MS / 1000;

    let (tx, rx) = crossbeam_channel::bounded::<i16>(samples_per_frame as usize);
    tx.send(i16::equilibrium()).unwrap(); // send a value to make signal happy
    let audio_signal = sample::signal::from_iter(
        rx.into_iter().map(|v| [v.to_sample::<f64>()])
    );

    let ring_buffer = sample::ring_buffer::Fixed::from([[0.0]; 50]);
    let sinc = sample::interpolate::Sinc::new(ring_buffer);
    let mut resampled_signal = audio_signal.from_hz_to_hz(
        sinc,
        sample_rate as f64,
        dst_sample_rate as f64,
    );

    ev.play_stream(stream);
    let _audio_thread = std::thread::spawn(move || {
        ev.run(move |_stream, data| {
            match data {
                StreamData::Output { buffer: UOut::U16(mut buffer) } => {
                    for sample in buffer.chunks_mut(sample_format.channels as usize) {
                        let v = resampled_signal.next()[0];
                        for out in sample.iter_mut() {
                            *out = v.to_sample::<u16>();
                        }
                    }
                },
                StreamData::Output { buffer: UOut::I16(mut buffer) } => {
                    for sample in buffer.chunks_mut(sample_format.channels as usize) {
                        let v = resampled_signal.next()[0];
                        for out in sample.iter_mut() {
                            *out = v.to_sample::<i16>();
                        }
                    }
                },
                StreamData::Output { buffer: UOut::F32(mut buffer) } => {
                    for sample in buffer.chunks_mut(sample_format.channels as usize) {
                        let v = resampled_signal.next()[0];
                        for out in sample.iter_mut() {
                            *out = v.to_sample::<f32>();
                        }
                    }
                },

                _=> (),
            };
        })
    });


    // set up sdl
    let sdl_context = sdl2::init().expect("could not init sdl");
    let video = sdl_context.video().expect("could not get sdl video subsystem");

    let window = video.window("cool vis thing", 1920, 1080)
        .position_centered()
        .opengl()
        .build()
        .expect("could not build window");

    let mut canvas = window
        .into_canvas()
        .index(find_sdl_gl_driver().expect("no available gl driver"))
        .build()
        .expect("could not create canvas");

    gl::load_with(|name| video.gl_get_proc_address(name) as *const _);
    canvas.window().gl_set_context_to_current().expect("could not set gl context current");

    canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    let mut events = sdl_context.event_pump().expect("could not get sdl events");

    // do the shit
    let mut data_idx = 0;
    let mut frame = 0;
    let samples_per_framerate = sample_rate / FRAMERATE;
    'main: while data_idx < samples.len() {
        for v in &samples[data_idx..data_idx+samples_per_framerate as usize] {
            tx.send(*v).expect("could not send audio data to playback thread");
        }

        for event in events.poll_iter() {
            match event {
                sdl2::event::Event::Quit { .. } => break 'main,
                _ => (),
            }
        }

        let time = std::time::Instant::now();

        let (ch1_shift, _length, _corr) = find_frame_shift(&ch1_samples, data_idx, sample_rate);
        let (ch2_shift, _length, _corr) = find_frame_shift(&ch2_samples, data_idx, sample_rate);
        let (ch3_shift, _length, _corr) = find_frame_shift(&ch3_samples, data_idx, sample_rate);

        dbg!(time.elapsed());

        let ch1_start = (data_idx as i64 + ch1_shift as i64).max(0) as usize;
        let ch2_start = (data_idx as i64 + ch2_shift as i64).max(0) as usize;
        let ch3_start = (data_idx as i64 + ch3_shift as i64).max(0) as usize;

        let ch1_slice = &ch1_samples[ch1_start..ch1_start+samples_per_frame as usize];
        let ch2_slice = &ch2_samples[ch2_start..ch2_start+samples_per_frame as usize];
        let ch3_slice = &ch3_samples[ch3_start..ch3_start+samples_per_frame as usize];

        canvas.set_draw_color(sdl2::pixels::Color::RGB(0, 0, 0));
        canvas.clear();

        canvas.set_draw_color(sdl2::pixels::Color::RGB(128, 128, 128));
        canvas.draw_line(
            sdl2::rect::Point::new(0, 180),
            sdl2::rect::Point::new(1920, 180),
        ).expect("could not draw midline");
        canvas.draw_line(
            sdl2::rect::Point::new(0, 540),
            sdl2::rect::Point::new(1920, 540),
        ).expect("could not draw midline");
        canvas.draw_line(
            sdl2::rect::Point::new(0, 900),
            sdl2::rect::Point::new(1920, 900),
        ).expect("could not draw midline");


        canvas.set_draw_color(sdl2::pixels::Color::RGB(255, 255, 255));
        canvas.draw_line(
            sdl2::rect::Point::new(0, 360),
            sdl2::rect::Point::new(1920, 360),
        ).expect("could not draw channel lines");
        canvas.draw_line(
            sdl2::rect::Point::new(0, 720),
            sdl2::rect::Point::new(1920, 720),
        ).expect("could not draw channel lines");

        canvas.draw_lines(ch1_slice.iter().enumerate().map(|(n, v)| {
            sdl2::rect::Point::new(
                (n as f32 / samples_per_frame as f32 * 1920.) as i32,
                180 + *v as i32 / 100,
            )
        }).collect::<Vec<_>>().as_slice()).expect("could not draw the sidwiz");
        canvas.draw_lines(ch2_slice.iter().enumerate().map(|(n, v)| {
            sdl2::rect::Point::new(
                (n as f32 / samples_per_frame as f32 * 1920.) as i32,
                540 + *v as i32 / 100,
            )
        }).collect::<Vec<_>>().as_slice()).expect("could not draw the sidwiz");
        canvas.draw_lines(ch3_slice.iter().enumerate().map(|(n, v)| {
            sdl2::rect::Point::new(
                (n as f32 / samples_per_frame as f32 * 1920.) as i32,
                900 + *v as i32 / 100,
            )
        }).collect::<Vec<_>>().as_slice()).expect("could not draw the sidwiz");

        /*
        canvas.set_draw_color(sdl2::pixels::Color::RGB(255, 0, 255));
        canvas.draw_line(
            sdl2::rect::Point::new(0, 460),
            sdl2::rect::Point::new(length as i32 / 2, 460),
        ).expect("could not draw autocorr length");

        canvas.set_draw_color(sdl2::pixels::Color::RGB(255, 0, 0));
        canvas.draw_lines(corr.iter().enumerate().map(|(n, v)| {
            sdl2::rect::Point::new(
                (n as f32 / samples_per_frame as f32 * 1280.) as i32,
                720 - *v as i32 / 10,
            )
        }).collect::<Vec<_>>().as_slice()).expect("could not autocorr res");
        */

        data_idx += samples_per_framerate as usize;
        frame += 1;

        if DUMP_FRAMES {
            let mut pixels = [0u8; 1920*1080*4];
            unsafe {
                gl::ReadPixels(0, 0, 1920, 1080, gl::RGBA, gl::UNSIGNED_BYTE, pixels.as_mut_ptr() as *mut _);
            };

            let flipped = pixels.chunks(1920*4).rev().flatten().cloned().collect::<Vec<u8>>();

            let file = std::fs::File::create(&format!("./frames/{:08}.png", frame))
                .expect("could not create file");
            let ref mut w = std::io::BufWriter::new(file);

            let mut encoder = png::Encoder::new(w, 1920, 1080);
            encoder.set(png::ColorType::RGBA).set(png::BitDepth::Eight);
            let mut writer = encoder.write_header().expect("could not write png header");

            writer.write_image_data(&flipped).expect("could not write frame");
        }

        canvas.present();
    }
}
