# beeshvis

The worst oscilloscope visualization tool ever. (except for the part where its
written in rust 😉)

## [Demo](https://youtu.be/bzURTysHqsA)

## Centering algorithm

`beeshvis` uses a *really* experimental centering algorithm based on a modified
version of [bitstream autocorrelation](https://www.cycfi.com/2018/03/fast-and-efficient-pitch-detection-bitstream-autocorrelation/),
using the result of it to find the phase of the fundimental frequency using
a Fourier transform. I'm too lazy to go into my changes to the autocorrelation
algorithm, but you can try reading the code to find out yourself if you want.
In the end my changes make it quite a bit faster (haven't actually measured) and
introduces a novel technique to avoiding octave errors on "pure" waves that
often occur in chip music.

## Usage

Don't.

If you actually want to try it then most of the values you will want to change
are at the top of `src/main.rs`. There are a few more values you can tweak that
are scattered throughout the code and probably don't have any comments on them,
so good luck.

## Q/A

**What is it actually centering on?**: I actually don't know. FFT = basically
magic.

**I tried it and its SLOW!**: Yeah, everything happens on one thread (laziness +
later plans), and dumping every frame to a PNG is very expensive.

**Why do seemingly trivial waves sometimes fuck up?**: Most of the time it's
because the autocorrelation still isn't perfect, but sometimes the Fourier
transform gets a little strange.

**Why do some waves (usually FM) slide around?**: I don't know, blame Fourier.

